Chatbox version 1.01

Created by Rafid Chairullah - rafidchairullah@gmail.com

an independent chat module made using Spring framework on Java programming language. 

requirement:
- XAMPP or alike (MySQL)
- Java JDK 8
- IDE (recommended: Eclipse Oxygen or Intellij Idea)

Installation:
- download the project and open it in your IDE
- create database named "db_chatbox" in your localhost
- check your application-local.yml and adjusted the configuration make sure the connection work properly
- open the Run configuration
- type "local" in active profile
- run the program will create the database and go to your browser
- http://localhost:8585/swagger-ui.html#/ will access the swagger which is the UI used to see the application


Features:
- Create Private Message between 2 accounts
- Create Multi-person chat (1 or more)
- Sending message to chat
- Delete existing message from chat
- Edit existing message from chat
- Send image
- download image


Future Plans:
- search message feature