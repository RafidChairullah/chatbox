package com.example.chatbox.common;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
public class Constant {

    //-------------------------------------------
    //ROLE
    //-------------------------------------------
    public static final String ROLE_MEMBER = "ROLE_MEMBER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";

    public static final String ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DETAILED_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSSSS";

}
