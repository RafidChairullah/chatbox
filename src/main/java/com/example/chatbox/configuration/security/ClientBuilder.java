package com.example.chatbox.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.builders.ClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 23/10/2019
 */
@Configuration
public class ClientBuilder {

    //Web Client
    @Value("${app.ouauth2.web.id}")
    private String webId;

    @Value("${app.ouauth2.web.secret}")
    private String webSecret;

    @Value("${app.ouauth2.web.grant-types}")
    private String[] webGrantTypes;

    @Value("${app.ouauth2.web.roles}")
    private String[] webRoles;

    @Value("${app.ouauth2.web.resids}")
    private String[] webResids;

    @Value("${app.ouauth2.web.scopes}")
    private String[] webScopes;

    @Value("${app.ouauth2.web.autoapprove}")
    private boolean webAutoApprove;

    @Value("${app.ouauth2.web.valid-token}")
    int webValidToken;

    @Value("${app.ouauth2.web.valid-token-refresh}")
    int webValidTokenRefresh;

    //Mobile Client
    @Value("${app.ouauth2.mob.id}")
    private String mobId;

    @Value("${app.ouauth2.mob.secret}")
    private String mobSecret;

    @Value("${app.ouauth2.mob.grant-types}")
    private String[] mobGrantTypes;

    @Value("${app.ouauth2.mob.roles}")
    private String[] mobRoles;

    @Value("${app.ouauth2.mob.resids}")
    private String[] mobResids;

    @Value("${app.ouauth2.mob.scopes}")
    private String[] mobScopes;

    @Value("${app.ouauth2.mob.autoapprove}")
    private boolean mobAutoApprove;

    @Value("${app.ouauth2.mob.valid-token}")
    int mobValidToken;

    @Value("${app.ouauth2.mob.valid-token-refresh}")
    int mobValidTokenRefresh;

    @Bean
    public InMemoryClientDetailsServiceBuilder builder() {
        InMemoryClientDetailsServiceBuilder serviceBuilder = new InMemoryClientDetailsServiceBuilder();
        final ClientDetailsServiceBuilder<InMemoryClientDetailsServiceBuilder>.ClientBuilder clientBuilder = serviceBuilder
                .withClient(webId)
                .secret(encoder.encode(webSecret))
                .authorizedGrantTypes(webGrantTypes)
                .authorities(webRoles)
                .resourceIds(webResids)
                .scopes(webScopes)
                .autoApprove(webAutoApprove)
                .accessTokenValiditySeconds(webValidToken)
                .refreshTokenValiditySeconds(webValidTokenRefresh)

                .and()
                .withClient(mobId)
                .secret(encoder.encode(mobSecret))
                .authorizedGrantTypes(mobGrantTypes)
                .authorities(mobRoles)
                .resourceIds(mobResids)
                .scopes(mobScopes)
                .autoApprove(mobAutoApprove)
                .accessTokenValiditySeconds(mobValidToken)
                .refreshTokenValiditySeconds(mobValidTokenRefresh);

        return serviceBuilder;
    }

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Bean
    DefaultOAuth2RequestFactory defaultOAuth2RequestFactory() throws Exception {
        return new DefaultOAuth2RequestFactory(clientDetailsService);
    }

}
