package com.example.chatbox.controller;

import com.example.chatbox.dto.account.AccountCreateRequest;
import com.example.chatbox.dto.account.AccountDTO;
import com.example.chatbox.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@RestController
@RequestMapping("/api")
public class AccountController {

    @Autowired
    private AccountService service;

    @GetMapping("/account")
    public AccountDTO getAccountInformation(){
        return service.getUserInformation();
    }

    @PostMapping("/account")
    public ResponseEntity registerAccount(@RequestBody @Valid AccountCreateRequest request){
        service.registerAccount(request);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PatchMapping("/account/activate")
    public ResponseEntity verifyAccount(String code){
        service.activateAccount(code);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PatchMapping("/account/deactivate")
    public ResponseEntity deactivateAccount(){
        service.deactivateAccount();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
