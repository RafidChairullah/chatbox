package com.example.chatbox.controller;

import com.example.chatbox.dto.conversation.ConversationDTO;
import com.example.chatbox.dto.conversation.MessageCreateRequest;
import com.example.chatbox.service.ConversationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@RestController
@RequestMapping("/api")
public class ConversationController {

    @Autowired
    private ConversationService service;

    @GetMapping("/conversations")
    public List<ConversationDTO> getAllCurrentConversation(){
        return service.getAllCurrentConversation();
    }

    @GetMapping("/conversations/{id}")
    public ConversationDTO getConversationWithId(@PathVariable Integer id){
        return service.getConversationWithId(id);
    }

    @PostMapping("/conversations")
    public ResponseEntity createGroupChat(@RequestParam List<Integer> ids){
        service.createGroupChat(ids);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("conversations/{id}")
    public ResponseEntity createPersonalChat(@PathVariable Integer id){
        service.createPersonalChat(id);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/conversations/{id}/members/add")
    public ResponseEntity addUserToConversation(@PathVariable Integer id ,@RequestParam List<Integer> ids){
        service.addUserToConversation(id, ids);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/conversations/{id}/send")
    public ResponseEntity sendMessageToConversation(@PathVariable Integer id, @RequestBody @Valid MessageCreateRequest request){
        service.sendMessageToConversation(id, request);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PatchMapping("/conversations/{id}/{messageId}")
    public ResponseEntity editMessageFromConversation(@PathVariable Integer id, @PathVariable Integer messageId, @RequestParam String content){
        service.editMessageFromConversation(id, messageId, content);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PatchMapping("/conversations/{id}/members/{memberId}")
    public ResponseEntity removeMemberFromConversation(@PathVariable Integer id, @PathVariable Integer memberId){
        service.removeMemberFromConversation(id, memberId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
