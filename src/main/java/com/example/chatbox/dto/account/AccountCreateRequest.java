package com.example.chatbox.dto.account;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@Data
public class AccountCreateRequest {

    @NotBlank
    @Pattern(regexp = "^[_A-z]*((-|\\s)*[_A-z])*$", message = "error.name-cannot-include-special-characters")
    @NotNull(message = "error.name-cannot-be-null")
    private String fullName;

    @NotBlank
    @Email(message = "error.invalid-email-pattern")
    @NotNull(message = "error.email-cannot-be-empty")
    private String email;

    @NotBlank
    @NotNull(message = "error.password-cannot-be-empty")
    private String password;
}
