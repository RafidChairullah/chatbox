package com.example.chatbox.dto.account;

import lombok.Data;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@Data
public class AccountDTO {
    private Integer id;
    private String fullname;
    private String userName;
    private String email;
}
