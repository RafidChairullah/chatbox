package com.example.chatbox.dto.conversation;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@Data
public class ConversationDTO {

    private List<MemberDTO> members = new ArrayList<>();
    private List<MessageDTO> listMessages = new ArrayList<>();
}
