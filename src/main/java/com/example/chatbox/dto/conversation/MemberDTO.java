package com.example.chatbox.dto.conversation;

import lombok.Data;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 20/01/2020
 */
@Data
public class MemberDTO {
    private Integer id;
    private String username;
}
