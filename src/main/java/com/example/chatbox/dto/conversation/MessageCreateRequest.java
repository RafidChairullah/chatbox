package com.example.chatbox.dto.conversation;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@Data
public class MessageCreateRequest {
    @NotNull
    @NotBlank
    private String message;
}
