package com.example.chatbox.dto.conversation;

import com.example.chatbox.common.Constant;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@Data
public class MessageDTO {

    private Integer id;
    private Integer senderId;

    @JsonFormat(pattern = Constant.DETAILED_DATE_FORMAT)
    private Date date;

    private String message;
}
