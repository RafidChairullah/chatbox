package com.example.chatbox.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@Entity
@Data
@Table(name = "m_authority")
public class Authority implements GrantedAuthority {

    @Id
    private String code;

    @NotNull
    private String authority;

}
