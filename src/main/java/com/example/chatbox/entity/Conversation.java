package com.example.chatbox.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "m_conversation")
public class Conversation extends BaseEntity implements Serializable {

    @JsonIgnore
    @ManyToMany()
    private List<User> users = new ArrayList<>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "conversation")
    private List<Message> messages = new ArrayList<>();

}
