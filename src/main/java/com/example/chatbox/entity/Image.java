package com.example.chatbox.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 24/01/2020
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "t_image")
public class Image extends BaseEntity {
    private String url;
}
