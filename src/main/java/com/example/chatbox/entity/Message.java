package com.example.chatbox.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "t_message")
public class Message extends BaseEntity implements Serializable {
    private String content;

    @JsonIgnore
    @ManyToOne
    private User sender;

    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Conversation conversation;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY)
    private List<Image> images = new ArrayList<>();
}
