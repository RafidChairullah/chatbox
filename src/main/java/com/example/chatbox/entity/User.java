package com.example.chatbox.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "m_user")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "user_type", discriminatorType = DiscriminatorType.STRING)
public class User extends BaseEntity implements UserDetails {

    @JsonIgnore
    private String password;
    private String fullName;
    private String email;

    private boolean enabled;

    @JsonIgnore
    private String activationCode;

    @JsonIgnore
    private String resetTokenCode;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @ManyToMany(cascade = CascadeType.MERGE)
    private Set<Authority> authorities = new HashSet<>();

    @Override
    @JsonIgnore
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @JsonIgnore
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @JsonIgnore
    public String getUsername() {
        return email;
    }
}
