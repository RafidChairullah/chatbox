package com.example.chatbox.enumeration;

public interface AbstractEnum<T> {
    T getAlias();
}
