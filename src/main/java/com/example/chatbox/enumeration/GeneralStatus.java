package com.example.chatbox.enumeration;

import com.fasterxml.jackson.annotation.JsonValue;

public enum GeneralStatus implements AbstractEnum<String> {

    ENABLE("E"),
    DISABLE("D");

    private String alias;

    GeneralStatus(String alias) {
        this.alias = alias;
    }

    @Override
    @JsonValue
    public String getAlias() {
        return alias;
    }



}
