package com.example.chatbox.enumeration;

import org.codehaus.jackson.annotate.JsonValue;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
public enum  OnlineStatus implements AbstractEnum<String> {
    ONLINE("ON"),
    OFFLINE("OFF");

    private String alias;

    OnlineStatus(String alias) { this.alias = alias;}

    @Override
    @JsonValue
    public String getAlias() {
        return alias;
    }
}
