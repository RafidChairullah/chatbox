package com.example.chatbox.enumeration.converter;


import com.example.chatbox.enumeration.AbstractEnum;

import javax.persistence.AttributeConverter;

public abstract class AbstractEnumConverter<T, R extends AbstractEnum<T>> implements AttributeConverter<R, T> {

    @Override
    public T convertToDatabaseColumn(R r) {
        return r.getAlias();
    }

    public abstract R[] getAllEnum();

    public abstract R getDefaultValue();

    @Override
    public R convertToEntityAttribute(T t) {
        R generalStatus = null;
        R[] enumList = getAllEnum();
        for (R status : enumList) {
            if (status.getAlias().equals(t)) {
                generalStatus = status;
                break;
            }
        }

        if (generalStatus == null) {
            generalStatus = getDefaultValue();
        }
        return generalStatus;
    }
}
