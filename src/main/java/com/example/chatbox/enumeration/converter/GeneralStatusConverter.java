package com.example.chatbox.enumeration.converter;


import com.example.chatbox.enumeration.GeneralStatus;

public class GeneralStatusConverter extends AbstractEnumConverter<String, GeneralStatus>{

    @Override
    public GeneralStatus[] getAllEnum() {
        return GeneralStatus.values();
    }

    @Override
    public GeneralStatus getDefaultValue() {
        return GeneralStatus.DISABLE;
    }
}
