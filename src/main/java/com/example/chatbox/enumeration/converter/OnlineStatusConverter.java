package com.example.chatbox.enumeration.converter;

import com.example.chatbox.enumeration.OnlineStatus;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
public class OnlineStatusConverter extends AbstractEnumConverter<String, OnlineStatus>{

@Override
public OnlineStatus[] getAllEnum() {
        return OnlineStatus.values();
        }

@Override
public OnlineStatus getDefaultValue() {
        return OnlineStatus.OFFLINE;
        }
}
