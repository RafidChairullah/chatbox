package com.example.chatbox.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 23/10/2019
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidParamException extends RuntimeException {
    public InvalidParamException(String message) {
        super(message);
    }
}
