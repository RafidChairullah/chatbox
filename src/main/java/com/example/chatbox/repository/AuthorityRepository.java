package com.example.chatbox.repository;

import com.example.chatbox.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
public interface AuthorityRepository extends JpaRepository<Authority, Integer> {
    Authority findTopByCode(String code);
}
