package com.example.chatbox.repository;

import com.example.chatbox.entity.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
public interface ConversationRepository extends JpaRepository<Conversation, Integer> {
    @Query("from Conversation c join c.users u where u.id = ?1")
    List<Conversation> findAllByUserIdExists(Integer id);

    @Query("from Conversation c join c.users u where c.id = ?1 and u.id = ?2")
    Optional<Conversation> findTopByIdAndUserId(Integer id, Integer userId);

    @Query("select case when count(c) > 0 then true else false end from Conversation c join c.users u where c.id= ?1 and u.id = ?2")
    boolean existsByIdAndUserId(Integer id, Integer userId);

}
