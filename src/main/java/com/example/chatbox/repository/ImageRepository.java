package com.example.chatbox.repository;

import com.example.chatbox.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 24/01/2020
 */
public interface ImageRepository extends JpaRepository<Image, Integer> {
}
