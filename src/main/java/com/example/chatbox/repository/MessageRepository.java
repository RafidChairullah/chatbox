package com.example.chatbox.repository;

import com.example.chatbox.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
public interface MessageRepository extends JpaRepository<Message, Integer> {

    Optional<Message> findTopByIdAndSenderId(Integer id, Integer senderId);
}
