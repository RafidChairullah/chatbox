package com.example.chatbox.repository;

import com.example.chatbox.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
public interface UserRepository extends JpaRepository<User, Integer> {

    User findTopById(Integer id);

    User findTopByEmail(String email);

    User findTopByActivationCode(String code);

    @EntityGraph(attributePaths = {"authorities"})
    @Query("select u from User u where  u.email=?1")
    User findTopByEmailEager(String email);
}
