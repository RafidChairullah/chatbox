package com.example.chatbox.service;

import com.example.chatbox.dto.account.AccountCreateRequest;
import com.example.chatbox.dto.account.AccountDTO;


/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
public interface AccountService {
    AccountDTO getUserInformation();
    void registerAccount(AccountCreateRequest request);
    void activateAccount(String code);
    void deactivateAccount();
}
