package com.example.chatbox.service;

import com.example.chatbox.dto.conversation.ConversationDTO;
import com.example.chatbox.dto.conversation.MessageCreateRequest;

import java.util.List;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
public interface ConversationService {
    List<ConversationDTO> getAllCurrentConversation();
    ConversationDTO getConversationWithId(Integer id);
    void createPersonalChat(Integer id);
    void createGroupChat(List<Integer> ids);
    void addUserToConversation(Integer id ,List<Integer> ids);
    void sendMessageToConversation(Integer id, MessageCreateRequest request);
    void editMessageFromConversation(Integer id, Integer messageId, String content);
    void removeMemberFromConversation(Integer id, Integer memberId);
}
