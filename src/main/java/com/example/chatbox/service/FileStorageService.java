package com.example.chatbox.service;

import com.example.chatbox.configuration.FileStorageProperties;
import com.example.chatbox.entity.Image;
import com.example.chatbox.exception.FileStorageException;
import com.example.chatbox.exception.InvalidParamException;
import com.example.chatbox.exception.NotFoundException;
import com.example.chatbox.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;
    private SecurityUtils securityUtils;

    @Value("${app.hostname}")
    private String host;

    private ImageRepository imageRepository;
    private FileStorageProperties fileStorageProperties;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties, SecurityUtils securityUtils, ImageRepository imageRepository) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }

        this.securityUtils = securityUtils;
        this.imageRepository = imageRepository;
        this.fileStorageProperties = fileStorageProperties;
    }

    public Image storeImage(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            String contentType = file.getContentType();
            if (!isSupportedImageContentType(contentType)) {
                throw new FileStorageException("error.invalid-type-only-jpg-jpeg-png");
            }

            int tahun = Calendar.getInstance().get(Calendar.YEAR);
            int bulan = Calendar.getInstance().get(Calendar.MONTH);
            int hari = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            int userId = securityUtils.getCurrentUser().getId();

            Path newPath = Paths.get(fileStorageLocation.toString(),
                    String.valueOf(tahun),
                    String.valueOf(bulan),
                    String.valueOf(hari),
                    String.valueOf(userId))
                    .toAbsolutePath().normalize();

            String p = Paths.get(fileStorageProperties.getUploadDir(),
                    String.valueOf(tahun),
                    String.valueOf(bulan),
                    String.valueOf(hari),
                    String.valueOf(userId))
                    .toString();

            try {
                Files.createDirectories(newPath);
            } catch (Exception ex) {
                throw new FileStorageException("error.cannot-cre-directory");
            }

            fileName = formatFileName(fileName);

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = newPath.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            p = p.replace(File.separatorChar, '/');

            Image image = new Image();
            image.setUrl(host + "/" + p + "/" + fileName);
            imageRepository.save(image);

            return image;
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new FileStorageException("error.store-file");
        } catch (Exception e) {
            e.printStackTrace();
            throw new InvalidParamException(e.getMessage());
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new NotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new NotFoundException("File not found " + fileName, ex);
        }
    }

    private boolean isSupportedImageContentType(String contentType) {
        return contentType.equals("image/png")
                || contentType.equals("image/jpg")
                || contentType.equals("image/jpeg");
    }

    /**
     * Hapus semua karakter
     *
     * @param fileName
     * @return
     */
    private String formatFileName(String fileName) {
        List<String> sss = new LinkedList<>(Arrays.asList(fileName.split("\\.")));
        int lastIdx = sss.size() - 1;
        String ext = sss.get(lastIdx);
        sss.remove(ext);
        String s = org.apache.commons.lang3.StringUtils.join(sss, "");
        s = s.replaceAll("[^A-Za-z0-9]", "").toLowerCase();
        return s + "." + ext;

    }
}