package com.example.chatbox.service;
import com.example.chatbox.entity.User;
import com.example.chatbox.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

@Component
public class SecurityUtils {

    @Autowired
    private UserRepository userRepository;

    public User getCurrentUser() {
        OAuth2Authentication principal = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();

        User user = null;
        if (principal != null) {
            user = userRepository.findTopByEmailEager(String.valueOf(principal.getUserAuthentication().getPrincipal()));
        }
        return user;
    }
}