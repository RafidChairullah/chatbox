package com.example.chatbox.service;

import com.example.chatbox.entity.Authority;
import com.example.chatbox.entity.User;
import com.example.chatbox.repository.AuthorityRepository;
import com.example.chatbox.repository.UserRepository;
import com.example.chatbox.common.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@Component
public class Starter implements CommandLineRunner {

    @Autowired
    private AuthorityRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        //Add default roles
        initAuthorities();

        //Add testing user
        initDummyUser();
    }

    private void initDummyUser(){
        String email = "admin@chatbox.com";
        String password = "123456";

        User user = userRepository.findTopByEmail(email);
        Authority adminAuth = repository.findTopByCode(Constant.ROLE_ADMIN);

        if(user == null){
            user = new User();
            user.setEmail(email);
            user.setCreatedDate(new Date());
            user.setFullName("Admin");
            user.setEnabled(true);
            user.setPassword(passwordEncoder.encode(password));
            user.setAuthorities(new HashSet<>(Collections.singletonList(adminAuth)));
            userRepository.save(user);
        }
    }

    private void initAuthorities(){

        Authority authorityAdmin = repository.findTopByCode(Constant.ROLE_ADMIN);
        if(authorityAdmin == null){
            authorityAdmin = new Authority();
            authorityAdmin.setAuthority(Constant.ROLE_ADMIN);
            authorityAdmin.setCode(Constant.ROLE_ADMIN);
            repository.save(authorityAdmin);
        }

        Authority authorityMember = repository.findTopByCode(Constant.ROLE_MEMBER);
        if(authorityMember == null){
            authorityMember = new Authority();
            authorityMember.setAuthority(Constant.ROLE_MEMBER);
            authorityMember.setCode(Constant.ROLE_MEMBER);
            repository.save(authorityMember);
        }
    }
}
