package com.example.chatbox.service.impl;

import com.example.chatbox.common.Constant;
import com.example.chatbox.dto.account.AccountCreateRequest;
import com.example.chatbox.dto.account.AccountDTO;
import com.example.chatbox.entity.Authority;
import com.example.chatbox.entity.User;
import com.example.chatbox.exception.InvalidParamException;
import com.example.chatbox.exception.NotFoundException;
import com.example.chatbox.repository.AuthorityRepository;
import com.example.chatbox.repository.UserRepository;
import com.example.chatbox.service.AccountService;
import com.example.chatbox.service.SecurityUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 15/01/2020
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private SecurityUtils securityUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Override
    public AccountDTO getUserInformation() {

        User user = securityUtils.getCurrentUser();

        AccountDTO temp = new AccountDTO();

        temp.setId(user.getId());
        temp.setFullname(user.getFullName());
        temp.setUserName(user.getUsername());
        temp.setEmail(user.getEmail());

        return temp;
    }

    @Override
    public void registerAccount(AccountCreateRequest request) {
        User validUser = repository.findTopByEmail(request.getEmail());
        if (validUser != null) {
            throw new InvalidParamException("error.email-already-registered");
        }

        User user = new User();
        user.setFullName(request.getFullName());
        user.setEmail(request.getEmail());
        user.setEnabled(false);
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setCreatedDate(new Date());
        user.setActivationCode(RandomStringUtils.randomNumeric(20));


        repository.save(user);
    }

    @Override
    public void activateAccount(String code) {
        User user = repository.findTopByActivationCode(code);

        if(user == null){
            throw new NotFoundException("error.invalid-activation-code");
        }

        Authority authority = authorityRepository.findTopByCode(Constant.ROLE_MEMBER);

        user.setActivationCode(null);
        user.setEnabled(true);
        user.setAuthorities(new HashSet<>(Collections.singleton(authority)));
        repository.save(user);

    }

    @Override
    public void deactivateAccount() {
        User user = securityUtils.getCurrentUser();

        user.setEnabled(false);
        repository.save(user);
    }
}
