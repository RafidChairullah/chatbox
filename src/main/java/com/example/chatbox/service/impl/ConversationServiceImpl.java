package com.example.chatbox.service.impl;

import com.example.chatbox.dto.conversation.ConversationDTO;
import com.example.chatbox.dto.conversation.MemberDTO;
import com.example.chatbox.dto.conversation.MessageCreateRequest;
import com.example.chatbox.dto.conversation.MessageDTO;
import com.example.chatbox.entity.Conversation;
import com.example.chatbox.entity.Message;
import com.example.chatbox.entity.User;
import com.example.chatbox.exception.InvalidParamException;
import com.example.chatbox.exception.NotFoundException;
import com.example.chatbox.repository.ConversationRepository;
import com.example.chatbox.repository.MessageRepository;
import com.example.chatbox.repository.UserRepository;
import com.example.chatbox.service.ConversationService;
import com.example.chatbox.service.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Rafid Chairullah - rafidchairullah@gmail.com
 * 20/01/2020
 */
@Service
public class ConversationServiceImpl implements ConversationService {

    @Autowired
    private ConversationRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private SecurityUtils securityUtils;

    @Override
    public List<ConversationDTO> getAllCurrentConversation() {
        User user = securityUtils.getCurrentUser();

        List<Conversation> conversations = repository.findAllByUserIdExists(user.getId());

        return conversations.stream().map(conversation -> {
            ConversationDTO temp = new ConversationDTO();

            conversation.getUsers().forEach(user1 -> {
                MemberDTO tempUser = new MemberDTO();
                tempUser.setId(user1.getId());
                tempUser.setUsername(user1.getUsername());

                temp.getMembers().add(tempUser);
            });

            conversation.getMessages().forEach(message -> {
                MessageDTO tempMessage = new MessageDTO();

                tempMessage.setId(message.getId());
                tempMessage.setDate(message.getDate());
                tempMessage.setSenderId(message.getSender().getId());
                tempMessage.setMessage(message.getContent());

                temp.getListMessages().add(tempMessage);
            });
            return temp;
        }).collect(Collectors.toList());
    }

    @Override
    public ConversationDTO getConversationWithId(Integer id) {
        User user = securityUtils.getCurrentUser();

        Conversation conversation = repository.findTopByIdAndUserId(id, user.getId()).orElseThrow(() -> new NotFoundException("error.conversation-not-found"));

        ConversationDTO temp = new ConversationDTO();

        conversation.getUsers().forEach(user1 -> {
            MemberDTO tempUser = new MemberDTO();
            tempUser.setId(user1.getId());
            tempUser.setUsername(user1.getUsername());

            temp.getMembers().add(tempUser);
        });

        conversation.getMessages().forEach(message -> {
            MessageDTO tempMessage = new MessageDTO();

            tempMessage.setId(message.getId());
            tempMessage.setDate(message.getDate());
            tempMessage.setSenderId(message.getSender().getId());
            tempMessage.setMessage(message.getContent());

            temp.getListMessages().add(tempMessage);
        });

        return temp;
    }

    @Override
    public void createPersonalChat(Integer id) {
        User currentUser = securityUtils.getCurrentUser();
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundException("error.invalid-user-id"));

        Conversation conversation = new Conversation();

        conversation.getUsers().add(user);
        conversation.getUsers().add(currentUser);

        repository.save(conversation);
    }

    @Override
    @Transactional
    public void createGroupChat(List<Integer> ids) {
        User currentUser = securityUtils.getCurrentUser();
        Conversation conversation = new Conversation();

        if(ids != null && !ids.isEmpty()){
            ids.forEach(integer -> {
                User user = userRepository.findById(integer).orElseThrow(() -> new InvalidParamException("error.invalid-user-id"));
                conversation.getUsers().add(user);
            });
            if(!ids.contains(currentUser.getId())){
                conversation.getUsers().add(currentUser);
            }

            repository.save(conversation);

        }

    }

    @Override
    public void addUserToConversation(Integer id, List<Integer> ids) {
        User currentUser = securityUtils.getCurrentUser();
        Conversation conversation = repository.findTopByIdAndUserId(id, currentUser.getId()).orElseThrow(() -> new NotFoundException("error.conversation-not-found"));

        if(ids != null && !ids.isEmpty()){
            ids.forEach(uid -> {
                if (repository.existsByIdAndUserId(id, uid)) {
                    throw new InvalidParamException("error.this-user-already-in-the-chat");
                }
                User tempUser = userRepository.findTopById(uid);
                conversation.getUsers().add(tempUser);
            });
            repository.save(conversation);
        }
    }

    @Override
    public void sendMessageToConversation(Integer id, MessageCreateRequest request) {
        User user = securityUtils.getCurrentUser();
        Conversation conversation = repository.findTopByIdAndUserId(id, user.getId()).orElseThrow(() -> new NotFoundException("error.invalid-conversation-id"));

        Message message = new Message();

        message.setDate(new Date());
        message.setSender(user);
        message.setConversation(conversation);
        message.setContent(request.getMessage());
        messageRepository.save(message);

        conversation.getMessages().add(message);
        repository.save(conversation);

    }

    @Override
    public void editMessageFromConversation(Integer id, Integer messageId, String content) {
        User user = securityUtils.getCurrentUser();

        Message message = messageRepository.findTopByIdAndSenderId(messageId, user.getId()).orElseThrow(() -> new NotFoundException("error.no-message-found"));

        if(!message.getContent().equals(content)){
            message.setContent(content);
            messageRepository.save(message);
        }
    }

    @Override
    public void removeMemberFromConversation(Integer id, Integer memberId) {
        User currentUser = securityUtils.getCurrentUser();
        Conversation conversation = repository.findTopByIdAndUserId(id, currentUser.getId()).orElseThrow(() -> new NotFoundException("error.invalid-conversation-id"));
        User tempUser = userRepository.findById(memberId).orElseThrow(() -> new NotFoundException("error.invalid-user-id"));

        conversation.getUsers().forEach(user -> {
            if(user == tempUser) {
                conversation.getUsers().remove(user);
            }
        });
    }
}
